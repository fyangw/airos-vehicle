/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/mod_lib.h"

#include <dlfcn.h>

#include "framework/actor.h"
#include "framework/cutils.h"
#include "framework/log.h"
#include "framework/worker.h"

namespace air {
namespace link {

ModLib::ModLib() { pthread_rwlock_init(&rw_, NULL); }

ModLib::~ModLib() {
  for (const auto& p : mods_) {
    UnloadMod(p.first);
  }
  pthread_rwlock_destroy(&rw_);
}

std::string ModLib::GetModName(const std::string& full_path) {
  auto pos = full_path.find_last_of('/');
  pos = (pos == std::string::npos) ? -1 : pos;
  return full_path.substr(pos + 1);
}

bool ModLib::LoadMod(const std::string& dlpath) {
  auto dlname = GetModName(dlpath);
  pthread_rwlock_wrlock(&rw_);
  if (mods_.find(dlname) != mods_.end()) {
    pthread_rwlock_unlock(&rw_);
    DLOG(INFO) << dlname << " has loaded";
    return true;
  }

  void* dll_handle = dlopen(dlpath.c_str(), RTLD_NOW | RTLD_LOCAL);
  if (dll_handle == nullptr) {
    pthread_rwlock_unlock(&rw_);
    LOG(ERROR) << "Open dll " << dlpath << " failed, "
        << dlerror();
    return false;
  }
  mods_[dlname] = dll_handle;
  pthread_rwlock_unlock(&rw_);
  LOG(INFO) << "Load lib " << dlpath;
  return true;
}

bool ModLib::IsLoad(const std::string& dlname) {
  pthread_rwlock_rdlock(&rw_);
  auto res = mods_.find(dlname) != mods_.end();
  pthread_rwlock_unlock(&rw_);
  return res;
}

bool ModLib::UnloadMod(const std::string& dlname) {
  pthread_rwlock_wrlock(&rw_);
  if (mods_.find(dlname) == mods_.end()) {
    pthread_rwlock_unlock(&rw_);
    return true;
  }

  if (dlclose(mods_[dlname])) {
    LOG(ERROR) << "lib close failed, " << dlerror();
  }
  mods_.erase(dlname);
  pthread_rwlock_unlock(&rw_);
  return true;
}

std::shared_ptr<Worker> ModLib::CreateWorkerInst(
    const std::string& mod_name, const std::string& worker_name) {
  pthread_rwlock_rdlock(&rw_);
  if (mods_.find(mod_name) == mods_.end()) {
    LOG(ERROR) << "Find " << mod_name << "." << worker_name << " failed";
    return nullptr;
  }
  void* handle = mods_[mod_name];
  worker_create_func create =
      (worker_create_func)dlsym(handle, "worker_create");
  if (nullptr == create) {
    pthread_rwlock_unlock(&rw_);
    LOG(ERROR) << "Load " << mod_name << "." << worker_name
               << " module worker_create function failed";
    return nullptr;
  }
  auto worker = create(worker_name);
  worker->SetModName(mod_name);
  worker->SetTypeName(worker_name);
  pthread_rwlock_unlock(&rw_);
  return worker;
}

std::shared_ptr<Actor> ModLib::CreateActorInst(
    const std::string& mod_name, const std::string& actor_name) {
  pthread_rwlock_rdlock(&rw_);
  if (mods_.find(mod_name) == mods_.end()) {
    LOG(ERROR) << "Find " << mod_name << "." << actor_name << " failed";
    return nullptr;
  }
  void* handle = mods_[mod_name];
  actor_create_func create =
      (actor_create_func)dlsym(handle, "actor_create");
  if (nullptr == create) {
    pthread_rwlock_unlock(&rw_);
    LOG(ERROR) << "Load " << mod_name << "." << actor_name
               << " module actor_create function failed";
    return nullptr;
  }
  auto mod = create(actor_name);
  mod->SetModName(mod_name);
  mod->SetTypeName(actor_name);
  pthread_rwlock_unlock(&rw_);
  return mod;
}

}  // namespace link
}  // namespace air
