/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

namespace air {
namespace link {

enum class EventType : int {
  WORKER_COMMON,
  WORKER_TIMER,
  WORKER_USER,
  EVENT_CONN,
};

class Event {
 public:
  Event() {}
  virtual ~Event() {}

  /* 事件类型 */
  virtual EventType GetEventType() { return EventType::WORKER_USER; }

  /* 获得当前事件的文件描述符 */
  virtual int GetFd() = 0;

  /**
   * 监听的是文件描述符的写事件还是读事件
   * 一般是读或写事件(EPOLLIN/EPOLLOUT)
   */
  virtual unsigned int ListenEpollEventType() = 0;

  /* 获得的epoll事件类型(call by App) */
  virtual void RetEpollEventType(uint32_t ev) = 0;
};

}  // namespace link
}  // namespace air
