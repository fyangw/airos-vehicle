/******************************************************************************
 * Copyright 2022 The AIR Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <cstddef>
#include <iomanip>
#include <string>

namespace cw {
constexpr size_t kBufferSize = 4096;

std::string HexString(const std::string& str) {
  if (str.empty()) {
    return "";
  }
  std::stringstream ss;
  ss << std::hex;
  for (const auto& c : str) {
    ss << std::setw(2) << std::setfill('0') << (c & 0xFF);
  }
  return ss.str();
}

}  // namespace cw
