/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once

#include <memory>
#include <unordered_map>

#include <glog/logging.h>

#include "v2xpb-asn-bsm.pb.h"
#include "v2xpb-asn-map.pb.h"
#include "v2xpb-asn-position.pb.h"
#include "v2xpb-asn-spat.pb.h"

namespace v2x {
namespace scene {
class MsgVerify {
 public:
  bool VerifyBsm(const ::v2xpb::asn::Bsm& bsm);
  bool VerifyMap(const ::v2xpb::asn::Map& map);
  bool VerifySpat(const ::v2xpb::asn::Spat& spat);
  std::shared_ptr<::v2xpb::asn::SpatPhase> GetSpatPhaseId(
      const ::v2xpb::asn::SpatIntersection& spat_intersection,
      const ::v2xpb::asn::MapLane& lane);
};
}  // namespace scene
}  // namespace v2x
