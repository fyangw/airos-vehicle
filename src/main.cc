/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include <gflags/gflags.h>
#include <glog/logging.h>

#include "framework/log.h"
#include "air_link.h"
#include "v2xpb-asn-message-frame.pb.h"

void traffic_pb_func(const std::string& data) {
  v2xpb::asn::MessageFrame msg_f;
  msg_f.ParseFromString(data);
  LOG(INFO) << msg_f.DebugString();
}

void asn_deal_data(const std::string& data) {
  LOG(INFO) << "pb msg data: " << data;
}

void intersection_func(const std::string& data) {
  LOG(INFO) << "intersection msg data: " << data;
}

int main(int argc, char** argv) {
  google::ParseCommandLineFlags(&argc, &argv, true);

  air::link::Log log;

  air::link::AirLink air_link;
  air_link.Init();
  air_link.RegisterTrafficProtoCallBack(
    std::bind(&traffic_pb_func, std::placeholders::_1));
  air_link.RegisterAsnCallBack(
    std::bind(&asn_deal_data, std::placeholders::_1));
  air_link.Exec();

  while (1) {
    std::this_thread::sleep_for(
      std::chrono::seconds(1));
  }
  return 0;
}
