CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### source
FILE(GLOB SRCS "*.cpp" "*.cc" "*.c")

### lib
ADD_LIBRARY(air_pc5_service OBJECT ${SRCS})

TARGET_LINK_LIBRARIES(air_pc5_service
  air_grpc_pb
  air-link-asn-wrapper-grpc_pb
  air-link-asn-wrapper-asn_2
  air-link-asn-wrapper-asn_3
)
