/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once
#include <memory>
#include <string>
#include <thread>

#include "v2xpb-asn-message-frame.pb.h"

namespace air {
namespace link {
class App;
class Log;
class AirLink {
 public:
  /**
  * Initialization
  * @return true init success, false init fail
  */
  bool Init();

  /**
  * Register Traffic proto Format CallBack
  * @param traffic_pb_func function to deal proto format data
  * @return true register success, false register fail
  */
  bool RegisterTrafficProtoCallBack(
    const std::function<void(const std::string&)>& traffic_pb_func);

  /**
  * Register Traffic ASN Format CallBack
  * @param asn_func function to deal asn format data
  * @return true register success, false register fail
  */
  bool RegisterAsnCallBack(
    const std::function<void(const std::string&)>& asn_func);

  /**
  * Send Vehicle Basic Information
  * @param basic_info function to send vehicle basic data
  * @return true register success, false register fail
  */
  bool SendVehicleBasicInfo(const std::string& basic_info);

  /**
  * Start To Run
  * @return true exec success, false register fail
  */
  bool Exec();

 private:
  std::shared_ptr<air::link::App> app_;
  std::shared_ptr<std::thread> exec_func_;
};

}  // namespace link
}  // namespace air
