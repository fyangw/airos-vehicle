FROM docker.io/library/ubuntu:18.04
SHELL ["/bin/bash", "-c"]

ENV  DEBIAN_FRONTEND noninteractive
COPY tuna-ubuntu-1804-sources.list /etc/apt/sources.list

RUN apt  update
RUN apt  install -y --no-install-recommends                    \
         build-essential autoconf automake libtool-bin         \
         zip unzip bzip2 xz-utils gzip zstd sudo               \
         vim less file gawk lsof wget curl ssh iputils-ping    \
         ca-certificates git git-gui gitk git-lfs telnet       \
         libz-dev libbz2-dev liblz4-dev liblzma-dev libssl-dev \
         libjsoncpp-dev         libgflags-dev                  \
         libwebsocketpp-dev                                    \
         libboost-dev           libboost-system-dev            \
         libgeographic-dev      uuid-dev

RUN mkdir /opt/third-party
WORKDIR   /opt/third-party

ARG CMAKE_VERSION="3.24.3"
ARG PROTOBUF_VERSION="3.19.6"
ARG GTEST_VERSION="1.12.1"
ARG GLOG_VERSION="0.6.0"
ARG ASN1C_COMMIT_ID="9925dbbda86b436896108439ea3e0a31280a6065"

RUN wget --content-disposition \
    https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-linux-x86_64.sh \
    https://github.com/protocolbuffers/protobuf/archive/refs/tags/v${PROTOBUF_VERSION}.tar.gz                   \
    https://github.com/google/googletest/archive/refs/tags/release-${GTEST_VERSION}.tar.gz                      \
    https://github.com/google/glog/archive/refs/tags/v${GLOG_VERSION}.tar.gz                                    \
    https://github.com/vlm/asn1c/archive/${ASN1C_COMMIT_ID}.tar.gz

# ==============================================================================
RUN bash ./cmake-${CMAKE_VERSION}-linux-x86_64.sh --skip-license --prefix=/usr/local

RUN tar -xf protobuf-${PROTOBUF_VERSION}.tar.gz                     \
 && cd protobuf-${PROTOBUF_VERSION}                                 \
 && cmake ./cmake                                                   \
          -DCMAKE_POSITION_INDEPENDENT_CODE=ON                      \
          -DCMAKE_INSTALL_PREFIX=/usr/local                         \
          -Dprotobuf_BUILD_TESTS=OFF                                \
          -Dprotobuf_BUILD_LIBPROTOC=ON                             \
          -Dprotobuf_BUILD_SHARED_LIBS=OFF                          \
 && make -j$(nproc) install/strip clean                             \
 && rm CMakeCache.txt                                               \
 && cmake ./cmake                                                   \
          -DCMAKE_POSITION_INDEPENDENT_CODE=ON                      \
          -DCMAKE_INSTALL_PREFIX=/usr/local                         \
          -Dprotobuf_BUILD_TESTS=OFF                                \
          -Dprotobuf_BUILD_LIBPROTOC=ON                             \
          -Dprotobuf_BUILD_SHARED_LIBS=ON                           \
 && make -j$(nproc) install/strip clean                             \
 && cd ..                                                           \
 && rm -r protobuf-${PROTOBUF_VERSION}

RUN tar -xf googletest-release-${GTEST_VERSION}.tar.gz              \
 && cd googletest-release-${GTEST_VERSION}                          \
 && cmake .                                                         \
          -DCMAKE_POSITION_INDEPENDENT_CODE=ON                      \
          -DCMAKE_INSTALL_PREFIX=/usr/local                         \
 && make -j$(nproc) install                                         \
 && cd ..                                                           \
 && rm -r googletest-release-${GTEST_VERSION}

RUN tar -xf glog-${GLOG_VERSION}.tar.gz                             \
 && cd glog-${GLOG_VERSION}                                         \
 && cmake .                                                         \
          -DCMAKE_POSITION_INDEPENDENT_CODE=ON                      \
          -DCMAKE_INSTALL_PREFIX=/usr/local                         \
          -DWITH_GTEST=OFF                                          \
          -DWITH_THREADS=ON                                         \
          -DWITH_UNWIND=ON                                          \
          -DWITH_TLS=ON                                             \
          -DWITH_GFLAGS=OFF                                         \
 && make -j$(nproc) install/strip clean                             \
 && cd ..                                                           \
 && rm -r glog-${GLOG_VERSION}

RUN tar -xf asn1c-${ASN1C_COMMIT_ID}.tar.gz                         \
 && cd asn1c-${ASN1C_COMMIT_ID}                                     \
 && autoreconf -ivf                                                 \
 && ./configure --prefix=/usr/local                                 \
 && make -j$(nproc) install                                         \
 && cd ..                                                           \
 && rm -r asn1c-${ASN1C_COMMIT_ID}

RUN /sbin/ldconfig
