#!/usr/bin/env bash

readonly PWD0=$(readlink -fn "$(dirname "$0")")
set -e
cd "${PWD0}"
################################################################################
function BE_log_debug() {
  echo -e "\033[1;35m[DEBUG]" "$@" "\033[0m" >&2
}
function BE_log_info() {
  echo -e "\033[1;36m[INFO]" "$@" "\033[0m" >&2
}
function BE_log_warn() {
  echo -e "\033[1;33m[WARN]" "$@" "\033[0m" >&2
}
function BE_log_error() {
  echo -e "\033[1;31m[ERROR]" "$@" "\033[0m" >&2
}
function BE_log_fatal() {
  echo -e "\033[1;31m[FATAL]" "$@" "\033[0m" >&2
  exit 1
}
################################################################################
### Android
readonly BE_NDK_BIN_PATH="toolchains/llvm/prebuilt/linux-x86_64/bin"
function BE_fn_check_ndk() { (
  # shellcheck disable=SC2030
  if [[ -z "${ANDROID_NDK_ROOT}" ]]; then
    BE_log_fatal "ANDROID_NDK_ROOT is EMPTY!"
  fi
  if ! "${ANDROID_NDK_ROOT}/${BE_NDK_BIN_PATH}/clang" --version &>/dev/null; then
    BE_log_fatal "ANDROID_NDK_ROOT: ${ANDROID_NDK_ROOT} is INVALID!"
  fi
); }

function BE_fn_find_ndk() { (
  ANDROID_SDK_ROOT="${ANDROID_SDK_ROOT:-"${HOME}/Android/Sdk"}"
  if ! cd "${ANDROID_SDK_ROOT}/ndk" >/dev/null; then
    BE_log_fatal "ANDROID_SDK_ROOT: ${ANDROID_SDK_ROOT} is invalid."
  fi
  while read -r line; do
    # shellcheck disable=SC2030
    ANDROID_NDK_ROOT="${PWD}/$line"
    export ANDROID_NDK_ROOT
    BE_fn_check_ndk &&
      readlink -f "${ANDROID_NDK_ROOT}" &&
      exit 0
  done <<<"$(find . -maxdepth 1 -mindepth 1 -type d | sort -rV)"
  BE_log_fatal "Cannot find NDK in SDK: ${ANDROID_SDK_ROOT}"
); }

declare -r BE_ANDROID_SDK_LVL="23"
declare -rA BE_ANDROID_HOSTS=(
  ["x86_64"]="x86_64-linux-android${BE_ANDROID_SDK_LVL}"
  ["x86"]="i686-linux-android${BE_ANDROID_SDK_LVL}"
  ["arm64-v8a"]="aarch64-linux-android${BE_ANDROID_SDK_LVL}"
  ["armeabi-v7a"]="armv7a-linux-androideabi${BE_ANDROID_SDK_LVL}"
)

function main() {
  ##############################################################################
  if [[ -z "${BE_ANDROID_PREFIX}" ]]; then
    BE_log_fatal "BE_ANDROID_PREFIX is not set."
  fi
  # shellcheck disable=SC2031
  if [[ -z "${ANDROID_NDK_ROOT}" ]]; then
    ANDROID_NDK_ROOT="$(BE_fn_find_ndk)"
  fi
  BE_fn_check_ndk
  export PATH="${ANDROID_NDK_ROOT}/${BE_NDK_BIN_PATH}:${PATH}"
  export ANDROID_NDK_HOME="${ANDROID_NDK_ROOT}"
  ##############################################################################
  # THIS IS A CONFIG
  local arch="arm64-v8a"
  if [[ 0 -lt "$#" ]]; then
    arch="${1}"
    shift
  fi
  ##############################################################################
  local target="${BE_ANDROID_HOSTS[${arch}]}"
  local build_dir
  build_dir="$(mktemp -d ./build-XXXXXXXXXX)"
  cmake \
    --toolchain "${ANDROID_NDK_HOME}/build/cmake/android.toolchain.cmake" \
    -DANDROID_NATIVE_API_LEVEL="${BE_ANDROID_SDK_LVL}" \
    -DANDROID_ABI="${arch}" \
    -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY="NEVER" \
    -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE="NEVER" \
    -DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE="NEVER" \
    -DCMAKE_PREFIX_PATH="${BE_ANDROID_PREFIX}/${arch}" \
    -DCMAKE_SHARED_LINKER_FLAGS="-L${BE_ANDROID_PREFIX}/${arch}/lib" \
    -DCMAKE_EXE_LINKER_FLAGS="-L${BE_ANDROID_PREFIX}/${arch}/lib" \
    -B "${build_dir}" \
    -S .
  make -C "${build_dir}" -j"$(nproc)" >/dev/null
  make -C "${build_dir}" -j"$(nproc)" install >/dev/null
}

main "$@"
